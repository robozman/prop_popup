prop_popup
==========


Overview
--------
prop_popup is an application designed to display images on your screen for a short amount of time. This is useful for showing images that relate to media keys and other things.

prop_popup is written in C using GTK, GDK, GLIB, GIO, GDBus, Cairo, and Meson.


Icons
-----
Included icons are curtosy of https://github.com/Templarian/MaterialDesign.

These icons are avaliable under the SIL Open Font License, Version 1.1 which can be found at http://scripts.sil.org/OFL.

Build Dependencies
------------------
In additon to the dependencies listed above, prop_popup requires gimp and python3 to build. 

Example Commands
----------------

Here are some example commands to control prop_popup::

    $ dbus-send --session --type=method_call --dest=xyz.ohea.prop_popup /xyz/ohea/prop_popup xyz.ohea.prop_popup.display string:"volume-mute-rounded"
    $ dbus-send --session --type=method_call --dest=xyz.ohea.prop_popup /xyz/ohea/prop_popup xyz.ohea.prop_popup.display string:"volume-minus-rounded"
    $ dbus-send --session --type=method_call --dest=xyz.ohea.prop_popup /xyz/ohea/prop_popup xyz.ohea.prop_popup.display string:"volume-plus-rounded"
