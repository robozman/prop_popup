#!/bin/bash

printf "\
<?xml version="1.0" encoding="UTF-8"?>\n\
<gresources>\n\
  <gresource prefix=\"/org/gtk/prop_popup\">\n\
" > resources.xml
for file in "$@"
do
    printf "    <file preprocess=\"to-pixdata\" alias=\"${file##*/}\">$file</file>\n" >> resources.xml
done

printf "\
  </gresource>\n\
</gresources>\n\
" >> resources.xml
