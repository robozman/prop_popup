cons
=====

Licences
--------

``volume-minus.svg``, ``volume-plus.svg``, ``volume-high.svg``, and ``volume-mute.svg``
are from https://github.com/Templarian/MaterialDesign, licenced as follows:

- Templates - WTFPL

  - Available at http://www.wtfpl.net/

- Community Icons - SIL Open Font License 1.1

  - Available at http://scripts.sil.org/OFL

- Google Material Design Icons - Apache License 2.0

  - Available at https://github.com/google/material-design-icons/blob/master/LICENSE

Creation Process
----------------

1. Convert ``svg`` files to ``png`` files using inkscape

   ``inkscape -z -e output.png -w 400 -h 400 input.svg``

2. Add Rounded Background

- Open GIMP

- Import PNG

- Create blank background layer

  - Set opacity to 0 

- Create rounded rectangle layer

  - Select rounded rectange (Select -> Rounded Rectangle)

    - Radius 50

  - Fill with white

- Set opacity to 50

- Export as PNG
