#include <stdio.h>
#include <sys/stat.h>

#include <libconfig.h>

#include <gtk/gtk.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <gdk/gdkscreen.h>
#include <cairo.h>
#include <gio/gio.h>
#include <glib.h>
#include <glib/gprintf.h>

#include <prop_popup_interface.h>

static void screen_changed(GtkWidget *widget, GdkScreen *old_screen, gpointer user_data);
static gboolean expose_draw(GtkWidget *widget, GdkEventExpose *event, gpointer userdata);
static void on_name_acquired(GDBusConnection *connection, const gchar *name, gpointer user_data);


struct user_data {
    GtkWidget *window;
    GtkWidget *fixed_container;
    GtkImage *image;
    gboolean displayed;
    gchar *current_image;
    guint timeout_id;
    const gchar **system_data_dirs;
};

int main(int argc, char **argv) {

	// get path to config dir
    const gchar *user_config_dir = g_get_user_config_dir();
    const gchar *prop_popup_config_dir = g_build_filename(user_config_dir, "prop_popup", NULL);
	// create config dir
	int ret = g_mkdir_with_parents(prop_popup_config_dir,
					S_IRWXU | 
					S_IRGRP | S_IXGRP |
					S_IROTH | S_IXOTH);
	if (ret < 0) {
			g_fprintf(stderr, "Error creating config directory: %s\n", prop_popup_config_dir);
			return -1;
	}

	// get path to config file
    const gchar *prop_popup_config_file = g_build_filename(prop_popup_config_dir, "config", NULL);
	// create empty config file if none exists
	gboolean config_file_exists = g_file_test(prop_popup_config_file, G_FILE_TEST_EXISTS);
	if (!config_file_exists) {
			gboolean result = g_file_set_contents(prop_popup_config_file, "", -1, NULL);
	}

	// get path to user data dir images subdir
    const gchar *user_data_dir = g_get_user_data_dir();
    const gchar *prop_popup_images_data_dir = g_build_filename(user_data_dir, "prop_popup", "images", NULL);
	// create directory if it doesn't exist
	ret = g_mkdir_with_parents(prop_popup_images_data_dir,
					S_IRWXU | 
					S_IRGRP | S_IXGRP |
					S_IROTH | S_IXOTH);
	if (ret < 0) {
			g_fprintf(stderr, "Error creating local user images directory: %s\n", prop_popup_images_data_dir);
			return -1;
	}
   
   	g_free(prop_popup_config_dir);
	g_free(prop_popup_config_file);
	g_free(prop_popup_images_data_dir);	


    struct user_data *data = malloc(sizeof(struct user_data));
    memset(data, 0, sizeof(struct user_data));
    data->displayed = FALSE;

	// deep copy system_data_dirs array to user_data
    const gchar * const * system_data_dirs = g_get_system_data_dirs();
	int count = 0;
    for (const gchar * const *directory = system_data_dirs; *directory != NULL; directory++) {
		count++;   
	}
	// increment one extra slot for NULL terminator
	count++;

	// allocated array, adding an extra slot for the user data dir
	data->system_data_dirs = malloc(sizeof(gchar *) * (count + 2));
	for (int i = 0; i < count; i++) {
		data->system_data_dirs[i] = g_strdup(system_data_dirs[i]);
	}
	data->system_data_dirs[count - 1] = g_strdup(user_data_dir);


    gtk_init(&argc, &argv);

    g_bus_own_name(G_BUS_TYPE_SESSION, "xyz.ohea.prop_popup", G_BUS_NAME_OWNER_FLAGS_NONE, NULL,
		   on_name_acquired, NULL, data, NULL);
    
    gtk_main();

    return 0;
}

static gint
hide_image(gpointer user_data)
{
    struct user_data *data = (struct user_data*) user_data;

    gtk_widget_destroy(GTK_WIDGET(data->window));

	const gchar **system_data_dirs = data->system_data_dirs;

    memset(data, 0, sizeof(struct user_data));
    data->displayed = FALSE;
	data->system_data_dirs = system_data_dirs;
    return 0;
}

//static gint
//change_image(struct user_data *data, const gchar *image_name)
//{
//    g_source_remove(data->timeout_id);
//
//    data->current_image = g_strdup(image_name);
//    GdkPixbuf *image_pixbuf;
//
//    for (const gchar * const *directory = user_data->system_data_dirs; *directory != NULL; directory++) {
//        const gchar *file_path = g_build_filename(directory, "prop_popup", "images", "image_name", NULL);
//	image_pixbuf = gdk_pixbuf_new_from_file(file_path);
//	if (image_pixbuf) {
//		break;
//	}
//    }
//
//    if (!image_pixbuf) {
//	   return -1; 
//    }
//
//    gchar *resource_path = g_strdup_printf("/org/gtk/prop_popup/%s.png", image_name);
//
//    GdkPixbuf *volume_pixbuf = gdk_pixbuf_new_from_resource(resource_path, NULL);
//
//    if (!volume_pixbuf) {
//        g_fprintf(stderr, "Error loading %s.png from GResource\n", image_name);
//	gtk_widget_destroy(GTK_WIDGET(data->window));
//	return;
//    }
//
//    gtk_widget_destroy(GTK_WIDGET(data->image));
//
//    data->image = GTK_IMAGE(gtk_image_new_from_pixbuf(volume_pixbuf));
//
//    if (!data->image) {
//        g_fprintf(stderr, "Error loading volume_image from volume_pixbuf\n");
//	gtk_widget_destroy(GTK_WIDGET(data->window));
//	return;
//    }
//    gtk_widget_set_size_request(GTK_WIDGET(data->image), 400, 400);
//    gtk_container_add(GTK_CONTAINER(data->fixed_container), GTK_WIDGET(data->image));
//    gtk_widget_show_all(GTK_WIDGET(data->window));
//
//    data->timeout_id = g_timeout_add(1000, hide_image, data);
//    return 0;
//}


static gint
load_image(struct user_data *data, const gchar *image_name)
{
	GdkPixbuf *image_pixbuf = NULL;

    for (const gchar * const *directory = data->system_data_dirs; *directory != NULL; directory++) {
        const gchar *file_path = g_build_filename(*directory, "prop_popup", "images", image_name, NULL);
		image_pixbuf = gdk_pixbuf_new_from_file(file_path, NULL);

		// if image is valid, stop searching
		if (image_pixbuf) {
			g_printf("Found image at %s\n", file_path);
			break;
		}
	}

	// if image_pixbuff is still NULL after loop, report error 
	if (!image_pixbuf) {
			g_fprintf(stderr, "Error loading %s\n", image_name);
			return -1;
	}

	if (data->displayed) {
		gtk_widget_destroy(GTK_WIDGET(data->image));
	}

	data->image = GTK_IMAGE(gtk_image_new_from_pixbuf(image_pixbuf));
	g_object_unref(image_pixbuf);

	if (!data->image) {
		g_fprintf(stderr, "Error loading image from pixbuf\n");
		return -1;
	}
	return 0;
}

static gint
create_window(struct user_data *data, const gchar *image_name)
{
    data->window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_position(GTK_WINDOW(data->window), GTK_WIN_POS_CENTER);
    gtk_window_set_default_size(GTK_WINDOW(data->window), 400, 400);
    gtk_window_set_title(GTK_WINDOW(data->window), "prop_popup");
    g_signal_connect(G_OBJECT(data->window), "delete-event", gtk_main_quit, NULL);

    gtk_widget_set_app_paintable(data->window, TRUE);

    g_signal_connect(G_OBJECT(data->window), "draw", G_CALLBACK(expose_draw), NULL);
    g_signal_connect(G_OBJECT(data->window), "screen-changed", G_CALLBACK(screen_changed), NULL);

    gtk_window_set_decorated(GTK_WINDOW(data->window), FALSE);
    gtk_widget_add_events(data->window, GDK_BUTTON_PRESS_MASK);

    data->fixed_container = gtk_fixed_new();

    data->displayed = TRUE;	    
    
	return 0;
}

static gint
display_image(struct user_data *data, const gchar *image_name)
{
    data->current_image = g_strdup(image_name);
	
	
	gtk_container_add(GTK_CONTAINER(data->window), data->fixed_container);
    screen_changed(data->window, NULL, NULL);
	gtk_widget_set_size_request(GTK_WIDGET(data->image), 400, 400);
	gtk_container_add(GTK_CONTAINER(data->fixed_container), GTK_WIDGET(data->image));

	gtk_widget_show(GTK_WIDGET(data->image));
	gtk_widget_show_all(GTK_WIDGET(data->window));

	return 0;
}


static gint
set_timeout(struct user_data *data, const gchar *image_name, const gint16 duration)
{
	data->timeout_id = g_timeout_add(duration, hide_image, data);
	return 0;
}


static gboolean
on_handle_display (proppopupProp_popup *interface, GDBusMethodInvocation *invocation,
					const gchar *image_name, gint16 duration, const gpointer user_data)
{
    struct user_data *data = (struct user_data*) user_data;

	if (data->displayed) {
    	int is_changed = g_strcmp0(data->current_image, image_name);
        if (is_changed != 0) {
            g_printf("Changing the displayed image\n");
        	int ret = load_image(data, image_name);
        	if (ret < 0) { goto fail; }
        	ret = display_image(data, image_name);
        	if (ret < 0) { goto fail; }
        }
        else if (data->displayed == TRUE) {
            printf("Data already being displayed, reseting timeout\n");
        	g_source_remove(data->timeout_id);
        	int ret = set_timeout(data, image_name, duration);
        	if (ret < 0) { goto fail; }
        	data->timeout_id = g_timeout_add(duration, hide_image, data);
        }
	}
    else {
		int ret = load_image(data, image_name);
		if (ret < 0) { goto fail; }
		ret = create_window(data, image_name);
		if (ret < 0) { goto fail; }
        ret = display_image(data, image_name);
		if (ret < 0) { goto fail; }
		ret = set_timeout(data, image_name, duration);
		if (ret < 0) { goto fail; }
    }

    
    gchar *response;
    response = g_strdup_printf("Image %s requested.", image_name);
    prop_popup_prop_popup_complete_display(interface, invocation, response);
    g_free (response);
    return TRUE;

fail:
	g_fprintf(stderr, "Some shit broke\n");
	hide_image(data);
	return FALSE;
}

static void
on_name_acquired(GDBusConnection *connection, const gchar *name, gpointer user_data)
{
    proppopupProp_popup *interface;
    GError *error;

    interface = prop_popup_prop_popup_skeleton_new();
    g_signal_connect(interface, "handle-display", G_CALLBACK(on_handle_display), user_data);
    error = NULL;
    g_dbus_interface_skeleton_export(G_DBUS_INTERFACE_SKELETON(interface), connection, "/xyz/ohea/prop_popup", &error);
    return;
}

gboolean supports_alpha = FALSE;
static void screen_changed(GtkWidget *widget, GdkScreen *old_screen, gpointer userdata) {
    GdkScreen *screen = gtk_widget_get_screen(widget);
    GdkVisual *visual = gdk_screen_get_rgba_visual(screen);

    if (!visual) {
        printf("Your screen does not support alpha channels!\n");
        visual = gdk_screen_get_system_visual(screen);
        supports_alpha = FALSE;
    } else {
        printf("Your screen supports alpha channels!\n");
        supports_alpha = TRUE;
    }

    gtk_widget_set_visual(widget, visual);
}

static gboolean expose_draw(GtkWidget *widget, GdkEventExpose *event, gpointer userdata) {
    cairo_t *cr = gdk_cairo_create(gtk_widget_get_window(widget));

    if (supports_alpha) {
        printf("setting transparent window\n");
        cairo_set_source_rgba (cr, 1.0, 1.0, 1.0, 0.0);
	
    } else {
        printf("setting opaque window\n");
        cairo_set_source_rgb (cr, 1.0, 1.0, 1.0); 
    }

    cairo_set_operator (cr, CAIRO_OPERATOR_SOURCE);
    cairo_paint (cr);

    cairo_destroy(cr);

    return FALSE;
}
